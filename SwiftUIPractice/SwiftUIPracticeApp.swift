//
//  SwiftUIPracticeApp.swift
//  SwiftUIPractice
//
//  Created by henderson on 2022/12/28.
//

import SwiftUI

@main
struct SwiftUIPracticeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
